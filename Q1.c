#include <stdio.h>
#include <stdlib.h>

int main()
{
    int num;
    printf("Enter any number: ");
    scanf("%d", &num);

    if(num > 0)
    {
        printf("This is a POSITIVE number \n");
    }
    else if(num < 0)
    {
        printf("This is a NEGATIVE number \n");
    }
    else
    {
        printf("This number is ZERO \n");
    }
    return 0;
}
