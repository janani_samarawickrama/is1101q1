#include <stdio.h>
#include <stdlib.h>

int main()
{
    char ch;
    printf("Please enter an alphabet: \n");
    scanf("%c", &ch);

    if(ch == 'a' || ch == 'e' || ch == 'i' || ch == '0' || ch == 'u' ||
       ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U')
    {
        printf("%c is a VOWEL \n ", ch);
    }
    else
    {
        printf("%c is a CONSTANT \n ", ch);
    }
    return 0;
}
